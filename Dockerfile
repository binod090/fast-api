FROM python:3.10.6
# Image from dockerhub

ENV PYTHONUNBUFFERED 1
EXPOSE 8000
# Expose the port 8000 in which our application runs
WORKDIR /app
# Make /app as a working directory in the container
# Copy requirements from host, to docker container in /app
COPY ./requirements .
RUN apt-get update
# commands for necessary dependencies install
RUN pip install --upgrade pip
RUN pip install -r production.txt
RUN mkdir media
# Run the application in the port 8000

# Copy everything from ./src directory to /app in the container
COPY . .

# COPY .env .

