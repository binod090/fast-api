from fastapi import APIRouter, status, Response, HTTPException
import models
from database import SessionLocal
from schemas import BookModel

db = SessionLocal()

router = APIRouter(
    prefix="/book",
    tags=["book_app"]
)

@router.get("/")
def read_book():
    books = db.query(models.Book).all()
    print(books)
    return books    

@router.get('/get-by-id/{book_id}',status_code=200)
def get_by_id(book_id:int):
    book = db.query(models.Book).filter(models.Book.id==book_id)
    return book

@router.post('/add-book',status_code=status.HTTP_201_CREATED)
def add_book(request:BookModel,response:Response):
    if request.id==db.query(models.Book).filter(models.Book.id==request.id).first().id:
        return HTTPException(status_code=status.HTTP_201_CREATED,detail="Id already exists!")
    else:
        new_book = models.Book(
            id = request.id,
            book_name=request.book_name,
            description=request.description
        )
        db.add(new_book)
        db.commit()
        db.refresh(new_book)
        return new_book
    
    
