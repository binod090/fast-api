from fastapi import status, Response, APIRouter
from database import SessionLocal
import models
from schemas import UserModel
from passlib.context import CryptContext

router = APIRouter(
    prefix="/users",
    tags=["user_app"]
)
db = SessionLocal()



pswd_cxt = CryptContext(schemes=["bcrypt"], deprecated="auto")
    
@router.post('/create-user',status_code=status.HTTP_201_CREATED)
def create_user(request:UserModel,response:Response):
    hashed_password = pswd_cxt.hash(request.password)
    new_user = models.User(id=request.id,username=request.username,password=hashed_password,email=request.email)
    db.add(new_user)
    db.commit()
    db.refresh(new_user)
    return new_user