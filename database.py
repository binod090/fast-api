from sqlalchemy.orm import declarative_base,sessionmaker

from sqlalchemy import create_engine

# creating engine
engine = create_engine("postgresql+psycopg2://fas_api_user:password@localhost/fast_api_db",echo=True)

# for db
SessionLocal = sessionmaker(bind=engine)

# for models
Base = declarative_base()
