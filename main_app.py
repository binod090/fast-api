from fastapi import FastAPI
from database import SessionLocal
from app.book.routers import book
from app.users.routers import user

app = FastAPI()
db = SessionLocal()

# router
app.include_router(book.router)
app.include_router(user.router)

    
