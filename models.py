from sqlalchemy import String,Boolean,Column,Integer,ForeignKey
from sqlalchemy.orm import relationship
from database import Base,engine


# making connection 
def create_tables():
    Base.metadata.create_all(engine)

class Book(Base):
    __tablename__='book'
    id = Column(Integer,primary_key=True)
    book_name = Column(String(200))
    description=Column(String(200),) 
    # user_id = Column(Integer,ForeignKey("user.id"))
    # users = relationship("User")
    
    
class Autor(Base):
    __tablename__='author'
    id = Column(Integer,primary_key=True)
    name = Column(String(200))
    description=Column(String(200),) 


class User(Base):
    __tablename__='user'
    id = Column(Integer,primary_key=True)
    username = Column(String(200))
    password=Column(String(200),) 
    email=Column(String(200),)
    