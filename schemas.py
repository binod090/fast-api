from pydantic import BaseModel

class OurBaseModel(BaseModel):
    class Config:
        orm_mode=True

class BookModel(OurBaseModel):
    id:int
    book_name:str
    description:str
    
class UserModel(OurBaseModel):
    id:int
    username:str
    password:str
    email:str